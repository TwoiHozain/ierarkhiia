package Pac;

public class Square extends Figure {
    private double sidelength1;
    public Square (double sidelength){
        sidelength1=sidelength;
    }
    @Override
    double getArea() {
        return sidelength1*sidelength1;
    }
    @Override
    void drow() {
        System.out.println("Площадь квадрата");
    }
}
