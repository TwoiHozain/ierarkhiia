package Pac;

import java.util.ArrayList;

public class Main {
    public  static void main(String[] args){
        ArrayList <Figure> figures=new ArrayList();
        figures.add(new Square(2));
        figures.add(new Square(3));
        figures.add(new Square(5));

        figures.add(new ThriAngle(6,7));
        figures.add(new ThriAngle(1,4));

        figures.add(new Circule(5));
        figures.add(new Circule(10));

        for (int i=0;i<figures.size();i++){
            Figure figure=figures.get(i);
            figure.drow();
            System.out.println(figure.getArea());
        }
    }
}
